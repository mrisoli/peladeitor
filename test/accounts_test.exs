defmodule Peladeitor.AccountsTest do
  use Peladeitor.DataCase

  alias Peladeitor.Accounts
  alias Peladeitor.Accounts.User

  import Peladeitor.Factory

  @create_attrs %{email: "some email"}
  @update_attrs %{email: "some updated email"}
  @invalid_attrs %{email: nil}

  test "list_users/1 returns all users" do
    user = insert(:user)
    assert Accounts.list_users() == [user]
  end

  test "get_user returns the user with given id" do
    user = insert(:user)
    assert Accounts.get_user(user.id) == user
  end

  test "get_user returns nil when there is no user" do
    assert Accounts.get_user(0) == nil
  end

  test "get_user! returns raises error when it does not find user" do
    assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(0) end
  end

  test "create_user/1 with valid data creates a user" do
    assert {:ok, %User{} = user} = Accounts.create_user(@create_attrs)
    assert user.email == "some email"
  end

  test "create_user/1 with invalid data returns error changeset" do
    assert {:error, %Ecto.Changeset{}} = Accounts.create_user(@invalid_attrs)
  end

  test "update_user/2 with valid data updates the user" do
    user = insert(:user)
    assert {:ok, user} = Accounts.update_user(user, @update_attrs)
    assert %User{} = user
    assert user.email == "some updated email"
  end

  test "update_user/2 with invalid data returns error changeset" do
    user = insert(:user)
    assert {:error, %Ecto.Changeset{}} = Accounts.update_user(user, @invalid_attrs)
    assert user == Accounts.get_user!(user.id)
  end

  test "delete_user/1 deletes the user" do
    user = insert(:user)
    assert {:ok, %User{}} = Accounts.delete_user(user)
    assert_raise Ecto.NoResultsError, fn -> Accounts.get_user!(user.id) end
  end

  test "change_user/1 returns a user changeset" do
    user = insert(:user)
    assert %Ecto.Changeset{} = Accounts.change_user(user)
  end
end
