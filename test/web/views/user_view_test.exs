defmodule Peladeitor.Web.UserViewTest do
  use Peladeitor.Web.ConnCase, async: true

  test "renders user.json" do
    user = insert(:user)
    json = render(
      Peladeitor.Web.UserView,
      "user.json",
      user: user,
    )
    assert json == %{
      id: user.id, email: user.email
    }
  end
end
