defmodule Peladeitor.Web.ErrorViewTest do
  use Peladeitor.Web.ConnCase, async: true

  test "renders 404.json" do
    assert render(Peladeitor.Web.ErrorView, "404.json", []) ==
      %{errors: %{detail: "Page not found"}}
  end

  test "render 500.json" do
    assert render(Peladeitor.Web.ErrorView, "500.json", []) ==
      %{errors: %{detail: "Internal server error"}}
  end

  test "renders 401.json" do
    assert render(Peladeitor.Web.ErrorView, "401.json", []) ==
      %{errors: %{detail: "Could not login"}}
  end

  test "render any other" do
    assert render(Peladeitor.Web.ErrorView, "505.json", []) ==
      %{errors: %{detail: "Internal server error"}}
  end
end
