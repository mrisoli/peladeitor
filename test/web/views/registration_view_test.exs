defmodule Peladeitor.Web.RegistrationViewTest do
  use Peladeitor.Web.ConnCase, async: true

  test "renders create.json" do
    user = insert(:user)
    json = render(
      Peladeitor.Web.RegistrationView,
      "create.json",
      user: user,
    )
    assert json == %{
      data: %{id: user.id, email: user.email}
    }
  end
end
