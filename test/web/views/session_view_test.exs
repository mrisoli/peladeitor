defmodule Peladeitor.Web.SessionViewTest do
  use Peladeitor.Web.ConnCase, async: true

  test "renders login.json" do
    user = insert(:user)
    jwt = "jwtbinarystring"
    json = render(
      Peladeitor.Web.SessionView,
      "login.json",
      user: user,
      jwt: jwt,
      exp: Guardian.Utils.timestamp
    )
    assert json == %{
      user: %{id: user.id, email: user.email},
      token: jwt,
      exp: Guardian.Utils.timestamp
    }
  end
end
