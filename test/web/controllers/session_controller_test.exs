defmodule Peladeitor.Web.SessionControllerTest do
  use Peladeitor.Web.ConnCase

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "set user session token create", %{conn: conn} do
    user = insert(:user)
    conn = post conn, session_path(conn, :create), id: user.id
    assert json_response(conn, 200)
  end

  test "fails when user does not exist", %{conn: conn} do
    conn = post conn, session_path(conn, :create), id: 0
    assert json_response(conn, 401)
  end
end
