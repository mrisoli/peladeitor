defmodule Peladeitor.Web.RegistrationControllerTest do
  use Peladeitor.Web.ConnCase

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "create user with email", %{conn: conn} do
    email = "someone@peladeitor.com"
    conn = post conn, registration_path(conn, :create), email: email
    assert json_response(conn, 201)
  end

  test "fails when user already exists", %{conn: conn} do
    user = insert(:user)
    conn = post conn, registration_path(conn, :create), email: user.email
    assert json_response(conn, 409)
  end
end
