# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :peladeitor,
  ecto_repos: [Peladeitor.Repo]

# Configures the endpoint
config :peladeitor, Peladeitor.Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "TtMHJWtYHG7WV+Ge+3ApBiDkP1BDw543QiXwrH45wHe0+jLx5zoJC+Ccu6zMIM9d",
  render_errors: [view: Peladeitor.Web.ErrorView, accepts: ~w(json)]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :guardian, Guardian,
  issuer: "Peladeitor",
  ttl: { 30, :days },
  allowed_drift: 2000,
  verify_issuer: true,
  secret_key: System.get_env("GUARDIAN_SECRET_KEY") || "uWo33u9gI+YqqMNk5Ywh1fYb4Uv7Lejl9YJEO2InAFYxoogiEJ7oahsleL5NAZWB",
  serializer: Peladeitor.GuardianSerializer

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
