defmodule Peladeitor.GuardianSerializer do
  @moduledoc """
  Serializer for Guardian tokens
  """
  @behaviour Guardian.Serializer

  alias Peladeitor.Repo
  alias Peladeitor.Accounts.User

  def for_token(%User{} = user), do: {:ok, "User:#{user.id}"}
  def for_token(_), do: {:error, "Unknown resource type"}

  def from_token("User:" <> id), do: {:ok, Repo.get(User, id)}
  def from_token(_), do: {:error, "Unknown resource type"}
end
