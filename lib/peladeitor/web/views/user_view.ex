defmodule Peladeitor.Web.UserView do
  use Peladeitor.Web, :view

  def render("user.json", %{user: user}) do
    %{id: user.id, email: user.email}
  end
end
