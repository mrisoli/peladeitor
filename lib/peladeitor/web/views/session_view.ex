defmodule Peladeitor.Web.SessionView do
  use Peladeitor.Web, :view

  alias Peladeitor.Web.UserView

  def render("login.json", %{user: user, jwt: jwt, exp: exp}) do
    %{
      user: render_one(user, UserView, "user.json"),
      token: jwt,
      exp: exp
    }
  end
end
