defmodule Peladeitor.Web.RegistrationView do
  use Peladeitor.Web, :view

  alias Peladeitor.Web.UserView

  def render("create.json", %{user: user}) do
    %{data: render_one(user, UserView, "user.json")}
  end
end
