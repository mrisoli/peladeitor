defmodule Peladeitor.Web.Router do
  use Peladeitor.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
    plug Guardian.Plug.VerifyHeader
    plug Guardian.Plug.LoadResource
  end

  scope "/api", Peladeitor.Web do
    pipe_through :api

    post "/login", SessionController, :create
    post "/register", RegistrationController, :create
  end
end
