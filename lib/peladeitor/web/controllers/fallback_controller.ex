defmodule Peladeitor.Web.FallbackController do
  use Peladeitor.Web, :controller
  @dialyzer {:nowarn_function, call: 2}

  def call(conn, {:error, :unauthorized}) do
    conn
    |> put_status(:unauthorized)
    |> render(Peladeitor.Web.ErrorView, "401.json")
  end

  def call(conn, {:error, :conflict}) do
    conn
    |> put_status(:conflict)
    |> render(Peladeitor.Web.ErrorView, "422.json")
  end
end
