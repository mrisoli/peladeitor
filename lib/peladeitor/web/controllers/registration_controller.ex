defmodule Peladeitor.Web.RegistrationController do
  use Peladeitor.Web, :controller

  action_fallback Peladeitor.Web.FallbackController
  alias Peladeitor.Accounts
  alias Peladeitor.Accounts.User

  def create(conn, params) do
    with {:ok, user} <- Accounts.create_user(params),
    do: results(conn, user)
  end

  defp results(conn, user = %User{}) do
    conn
    |> put_status(:created)
    |> render("create.json", user: user)
  end
end
