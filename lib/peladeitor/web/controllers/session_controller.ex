defmodule Peladeitor.Web.SessionController do
  use Peladeitor.Web, :controller

  action_fallback Peladeitor.Web.FallbackController

  alias Guardian.Plug
  alias Peladeitor.Accounts
  alias Peladeitor.Accounts.User

  def create(conn, params) do
    with {:ok, user} <- Accounts.authenticate(params) do
      authenticate_user(conn, user)
    else _ ->
      {:error, :unauthorized}
    end
  end

  defp authenticate_user(conn, user = %User{}) do
    new_conn = Plug.api_sign_in(conn, user)
    jwt = Plug.current_token(new_conn)
    {:ok, claims} = Plug.claims(new_conn)
    exp = Integer.to_string(Map.get(claims, "exp"))
    new_conn
    |> put_resp_header("authorization", "Bearer #{jwt}")
    |> put_resp_header("x-expires", exp)
    |> render("login.json", user: user, jwt: jwt, exp: exp)
  end
end
