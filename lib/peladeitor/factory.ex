defmodule Peladeitor.Factory do
  @moduledoc """
  this file imports all factories
  """
  @dialyzer {:nowarn_function, fields_for: 1}
  use ExMachina.Ecto, repo: Peladeitor.Repo
  use Peladeitor.Accounts.Factories.UserFactory
end
