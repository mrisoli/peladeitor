defmodule Peladeitor.Schema do
  @moduledoc """
  Ecto Schema extension
  """
  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      import Ecto.Changeset
    end
  end
end
