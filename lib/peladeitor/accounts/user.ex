defmodule Peladeitor.Accounts.User do
  @moduledoc """
  Unique user account information
  This does not include password or authentication provider
  which are stored separately
  """
  use Peladeitor.Schema

  schema "accounts_users" do
    field :email, :string

    timestamps()
  end

  def changeset(schema, attrs) do
    schema
    |> cast(attrs, [:email])
    |> validate_required([:email])
  end
end
