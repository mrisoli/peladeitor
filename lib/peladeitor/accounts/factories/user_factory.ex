defmodule Peladeitor.Accounts.Factories.UserFactory do
  @moduledoc """
  factories to easily create users
  """
  defmacro __using__(_opts) do
    quote do
      def user_factory do
        %Peladeitor.Accounts.User{
          email: sequence(:email, &"email-#{&1}@example.com"),
        }
      end
    end
  end
end
