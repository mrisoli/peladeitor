defmodule Peladeitor.Repo.Migrations.AddIndexToUserEmail do
  use Ecto.Migration

  def change do
    create unique_index(:accounts_users, [:email])
  end
end
